package main

import scala.swing._
import scala.collection.mutable.Buffer
import java.awt.{ Graphics2D, Color}
import java.awt.image.BufferedImage
import javax.swing.{ImageIcon, JPanel}
import tools._

// class Canvas is responsible for painting given commands on a Graphics2D -object.

class Canvas(var width: Int, var height: Int) extends Panel {
  
  preferredSize = new Dimension(width, height)

  override def paintComponent(g: Graphics2D) {
    super.paintComponent(g)
    g.clearRect(0, 0, width, height)
    for (cmd <- CommandList.get) cmd.execute(g)
    if (CommandList.temp.isDefined) CommandList.temp.get.execute(g)
  }
  
  def inBounds(x: Int, y: Int): Boolean = x >= 0 && x < width && y >= 0 && y < height 
  
  def newImage() = {
    
    val wDialog = Dialog.showInput(new BorderPanel, "Enter image width:", initial="600")
    
      wDialog match {
      
        case Some(dwidth) => 
          if (dwidth.exists((x: Char) => !"0123456789".contains(x)) || dwidth.toInt <= 0) {
            Dialog.showMessage(new BorderPanel, "Invalid number format.", title="Error!")
          }
          else {
            val hDialog = Dialog.showInput(new BorderPanel, "Enter image height:", initial="400")
            hDialog match {
              case Some(dheight) =>
                if (dheight.exists((x: Char) => !"0123456789".contains(x)) || dheight.toInt <= 0) {
                  Dialog.showMessage(new BorderPanel, "Invalid number format.", title="Error!")
                 }
                else {
                  width = dwidth.toInt
                  height = dheight.toInt
                  import gui.GUI
                  val newGUI = new GUI
                  newGUI.show()
                }
              case None => Unit
            }
          }
         
        case None => Unit
        
      }
  }
  

}

