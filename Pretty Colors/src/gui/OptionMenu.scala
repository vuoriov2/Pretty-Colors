package gui
import swing._
import swing.event._
import java.awt.{ Graphics2D, Color}
import BorderPanel._
import TabbedPane._
import tools._
import main.Canvas

// class OptionMenu models a popup -menu for selecting a tool and changing it's parametres.

abstract class OptionMenu extends BorderPanel {
  def update(): Unit  // Updates the currently used tool.
  val tabs: TabbedPane
  val selectButton = new Button(Action("Select"){this.update()})
  layout(selectButton) = BorderPanel.Position.South
}

class PencilOptionMenu(c: Canvas) extends OptionMenu { 
  
  val tp = new ThicknessTab(this)
  val cp = new ColorTab(this)
  
  val tabs = new TabbedPane {
    pages += new Page(cp.label, cp)
    pages += new Page(tp.label, tp)
  }
  
  layout(tabs) = BorderPanel.Position.Center
  
  def update() = {
    Tooltip.set("Selected tool: Pencil")
    Tool.changeTo(new PrettyPencil(tp.selection, cp.selection, c))
  }

}

class LineOptionMenu(c: Canvas) extends OptionMenu {

  val tp = new ThicknessTab(this)
  val cp = new ColorTab(this)
  
  val tabs = new TabbedPane {
    pages += new Page(cp.label, cp)
    pages += new Page(tp.label, tp)
  }
  
  layout(tabs) = BorderPanel.Position.Center

  def update() =  {
    Tooltip.set("Selected tool: Line")
    Tool.changeTo(new PrettyLine(tp.selection, cp.selection, c))
  }
}

class RectangleOptionMenu(c: Canvas) extends OptionMenu {
  
  val tt = new ThicknessTab(this)
  val ct = new ColorTab(this)
  val ft = new FillTab(this)
  
  val tabs = new TabbedPane {
    pages += new Page(ct.label, ct)
    pages += new Page(tt.label, tt)
    pages += new Page(ft.label, ft)
  }
  
  layout(tabs) = BorderPanel.Position.Center

  def update() =  {
    Tooltip.set("Selected tool: Rectangle")
    Tool.changeTo(new PrettyRectangle(tt.selection, ct.selection, ft.selection, c))
  }
}

class EllipseOptionMenu(c: Canvas)  extends OptionMenu {
  
  val tt = new ThicknessTab(this)
  val ct = new ColorTab(this)
  val ft = new FillTab(this)
  
  val tabs = new TabbedPane {
    pages += new Page(ct.label, ct)
    pages += new Page(tt.label, tt)
    pages += new Page(ft.label, ft)
  }
  
  layout(tabs) = BorderPanel.Position.Center
  
  override def update() =  {
    Tooltip.set("Selected tool: Ellipse")
    Tool.changeTo(new PrettyEllipse(tt.selection, ct.selection, ft.selection,  c))
  }
}

abstract class OptionTab extends BorderPanel {
  def label: String // Attribute name: Thickness, Color, etc.
  def selected: Any // Selected attribute.
}

class ThicknessTab(opm: OptionMenu) extends OptionTab {
  
  def label = "Thickness"
  def selected = selection
  var selection = 8 // Default radius: 8
  
  val group = new ButtonGroup
  
  val small = new RadioButton("Small (4)") {
    action = Action("Small (4)") {
      selection = 4
      opm.update()
    }
  }
  
  val medium = new RadioButton("Medium (8)") {
    action = Action("Medium (8)") {
      selection = 8
      opm.update()
    }
  }
  
  val large = new RadioButton("Large (16)") {
    action = Action("Large (16)") {
      selection = 16
      opm.update()
    }
  }   
 
  val veryLarge = new RadioButton("Very Large (24)") {
    action = Action("Very Large (24)") {
      selection = 24
      opm.update()
    }
  }
  
  val extraLarge = new RadioButton("Extra Large (32)") {
    action = Action("Extra Large (32)") {
      selection = 32
      opm.update()
    }
  }
  
  
  val radioButtons = List(small, medium, large, veryLarge, extraLarge)
  group.buttons ++= radioButtons 
  group.select(medium) // Default choice: Medium    
  val buttons = new BoxPanel(Orientation.Vertical) {
    contents ++= radioButtons
  }

  layout(buttons) = Position.North
  
}

class ColorTab(opm: OptionMenu) extends OptionTab {
  def label = "Color"
  def selected = selection
  var selection = Color.black
  val colorChooser = new ColorChooser {
    reactions += {
      case ColorChanged(_, c0) =>
        foreground = c0
        selection = c0
        opm.update()
    }
  }
  layout(colorChooser) = Position.North
}

class FillTab(opm: OptionMenu) extends OptionTab {
  def label = "Layout"
  def selected = selection
  var selection = false 
  
  val choice = new ButtonGroup
  
  val dontFill = new RadioButton("Bordered") {
    action = Action("Bordered") {
      selection = false
      opm.update()
    }
  }
  
  
  val fill = new RadioButton("Filled") {
    action = Action("Filled") {
      selection = true
      opm.update()
    }
  }
  
  val radioButtons = List(dontFill, fill)
  choice.buttons ++= radioButtons 
  choice.select(dontFill) // Default choice: Bordered.
  val buttons = new BoxPanel(Orientation.Vertical) {
    contents ++= radioButtons
  }

  layout(buttons) = Position.North
  
}