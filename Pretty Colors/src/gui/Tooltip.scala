package gui
import scala.swing.TextField

object Tooltip extends TextField {
  def set(s: String) = text = s
  columns = 10
  text = ""
  editable = false
}