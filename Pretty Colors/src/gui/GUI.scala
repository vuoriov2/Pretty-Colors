/* TODO
 * Empty clicks trigger a command - fix undo system
 * current color to own tab?
 * 
 * Rect / Ellipse FILL ? Gradient?
 * 
 * Fix new image windows
 * 
 */

package gui

import scala.swing._
import scala.swing.BorderPanel.Position._
import javax.swing.{JPopupMenu}
import event._
import java.awt.{ Color, Graphics2D}
import scala.util.Random

import main.Canvas
import tools._

class GUI() {

  def top = new MainFrame { 
  
    title = "Pretty Colors"    
      
    
      
    // Create an empty image. 
    // Default image size: x = 600, y = 600.
    var canvas = new Canvas(600, 600) 
    size = new Dimension((canvas.width * 2).floor.toInt, (canvas.height * 2).floor.toInt)
    // Create the tooltip panel.
    val info = Tooltip
    
    contents = new BorderPanel {
      layout(canvas) = Center
      layout(info) = South
    }
  
    
    // Top menu: File / Edit ... etc.
    menuBar = new MenuBar {
      
      contents += new Menu("File") {
        contents += new MenuItem(Action("New Image") {
          canvas.newImage()
        })
      
        contents += new MenuItem(Action("Load Image") {
          ???
        })
        contents += new MenuItem(Action("Save state") {
          ???
        })
        contents += new MenuItem(Action("Export as .PNG") {
          ???
        })
        contents += new MenuItem(Action("Export as .JPG") {
          ???
        })
        contents += new MenuItem(Action("Settings") {
          ???
        })
        contents += new MenuItem(Action("Exit") {
          sys.exit(0)
        })
      }
      
      contents += new Menu("Edit") {
        contents += new MenuItem(Action("Undo (Z)") {
          CommandList.undo()
          canvas.repaint()
        })
        contents += new MenuItem(Action("Clear Image") {
          ???
        })
        contents += new MenuItem(Action("Resize Image") {
          ???
        })
      }
      
      contents += new Menu("Shapes") {
        
        contents += new Menu("Pencil") {
          contents += new PencilOptionMenu(canvas)
        }
        
        contents += new Menu("Line") {
           contents += new LineOptionMenu(canvas)
        }
        
        contents += new Menu("Rectangle") {
           contents += new RectangleOptionMenu(canvas)
        }
        
        contents += new Menu("Ellipse") {
           contents += new EllipseOptionMenu(canvas)
        }
      }
  
      contents += new Menu("Filters") {
        contents += new MenuItem(Action("TBI") {
          ???
        })
      }
    }
  
    // Events
    
    listenTo(canvas.mouse.clicks)
    listenTo(canvas.mouse.moves)
    listenTo(canvas.mouse.wheel)
    listenTo(info.keys)
  
    reactions += {
  
      case e: MouseMoved =>
        Tooltip.set((s"(x=${e.point.x}, y=${e.point.y}) "))
      
      case e: MouseClicked =>
        if (canvas.inBounds(e.point.x, e.point.y)) {
          if (Tool.selected.isDefined) {
            val cmd = Tool.selected.get.findCommand(e.point.x, e.point.y, "Click")
            if (cmd.isDefined) {
              cmd.get.addToList()
              repaint()
            }
          }
        }
        
      case e: MouseDragged =>
        Tooltip.set((s"(x=${e.point.x}, y=${e.point.y}) "))
        if (canvas.inBounds(e.point.x, e.point.y)) {
          if (Tool.selected.isDefined) {
            val cmd = Tool.selected.get.findCommand(e.point.x, e.point.y, "Drag")
            if (cmd.isDefined) {
             if (!cmd.get.isTemporary) cmd.get.addToList() else CommandList.setTemp(cmd.get)
              repaint()
            }
          }
        }
       
      case e: MousePressed =>
        if (canvas.inBounds(e.point.x, e.point.y)) {
          if (Tool.selected.isDefined) {
            val cmd = Tool.selected.get.findCommand(e.point.x, e.point.y, "Press")
            if (cmd.isDefined) {
              cmd.get.addToList()
              repaint()
            }
          }
        }
        
      case e: MouseReleased => 
        CommandList.clearTemp()
        if (canvas.inBounds(e.point.x, e.point.y)) {
          if (Tool.selected.isDefined) {
            val cmd = Tool.selected.get.findCommand(e.point.x, e.point.y, "Release")
            if (cmd.isDefined) {
              cmd.get.addToList()
              repaint()
            }
          }
        }
      case e: KeyPressed =>
        e.key match {
          case Key.Z => 
            CommandList.undo()
            canvas.repaint()
          case otherwise => Unit
        }
      }
  }
  
  def show(): Unit = {
    val window = top
    top.visible = true
  }
  
}


