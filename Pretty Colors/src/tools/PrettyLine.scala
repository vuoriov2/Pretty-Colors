package tools
import java.awt.{ Graphics2D, Color}
import main.Canvas

class PrettyLine(val r: Int, val color: java.awt.Color, val canvas: Canvas) extends Tool {
  
  def name = "Line"
  
  var start: Option[(Int, Int)] = None

  def findCommand(x: Int, y: Int, id: String) = {
    id match {
      case "Press"   => Some(LinePressCommand(x, y, this))
      case "Release" => Some(LineReleaseCommand(x, y, this))
      case "Drag"    => Some(LineDragCommand(x, y, this))
      case otherwise => None
    }
  }
  
}

import java.awt.BasicStroke

case class LineDragCommand(x: Int, y: Int, t: PrettyLine) extends Command {

  override def isTemporary = true
  
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y) && t.start.isDefined) {
      g.setColor(t.color)
      g.setStroke(new BasicStroke(t.r))
      g.drawLine(t.start.get._1, t.start.get._2, x, y)
      
    }
   
  }  
}

case class LineReleaseCommand(x: Int, y: Int, t: PrettyLine) extends Command {
  
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y) && t.start.isDefined && t.start.get != (x, y)) {
      g.setColor(t.color)
      g.setStroke(new BasicStroke(t.r))
      g.drawLine(t.start.get._1, t.start.get._2, x, y)  
    }

  }  
}
case class LinePressCommand(x: Int, y: Int, t: PrettyLine) extends Command {
  
  def execute(g: Graphics2D) = {
    t.start = Some((x, y))
  }
  
}
