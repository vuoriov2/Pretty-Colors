package tools

abstract class Tool {
  def name: String
  def findCommand(x: Int, y: Int, id: String): Option[Command]
} 

object Tool {
  var selected: Option[Tool] = None
  def changeTo(t: Tool): Unit = {
    selected = Some(t)
  }
}