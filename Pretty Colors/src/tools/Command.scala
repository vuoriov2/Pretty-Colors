package tools
import java.awt.Graphics2D
import scala.collection.mutable.Buffer

abstract class Command {
  
  def execute(g: Graphics2D): Unit
  
  def isTemporary: Boolean = false
  
  def addToList() = CommandList.add(this)
  
}

