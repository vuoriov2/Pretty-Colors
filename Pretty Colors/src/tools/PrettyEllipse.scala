package tools
import java.awt.{ Graphics2D, Color}
import java.awt.geom.Ellipse2D
import main.Canvas

class PrettyEllipse(val r: Int, val color: java.awt.Color, val fill: Boolean, val canvas: Canvas) extends Tool {
  
  def name = "Rectangle"
  
  var start: Option[(Int, Int)] = None

  def draw(x: Int, y: Int, width: Int, height: Int, fill: Boolean, g: Graphics2D) = {
    val ellipse = new Ellipse2D.Double(x, y, width, height)
    g.draw(ellipse) 
    if (fill) {
      g.setPaint(this.color); // a dull blue-green
      g.fill(ellipse);
    }
  }
  
  def findCommand(x: Int, y: Int, id: String) = {
    id match {
      case "Press"   => Some(EllipsePressCommand(x, y, this))
      case "Release" => Some(EllipseReleaseCommand(x, y, this))
      case "Drag"    => Some(EllipseDragCommand(x, y, this))
      case otherwise => None
    }
  }
  
}

import java.awt.BasicStroke

case class EllipseDragCommand(x: Int, y: Int, t: PrettyEllipse) extends Command {

  override def isTemporary = true
  
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y) && t.start.isDefined) {
      g.setColor(t.color)
      g.setStroke(new BasicStroke(t.r))
      if (x - t.start.get._1 > 0 && y - t.start.get._2 > 0) t.draw(t.start.get._1, t.start.get._2, x - t.start.get._1 , y - t.start.get._2, t.fill, g)
      else if (x - t.start.get._1 > 0 && y - t.start.get._2 < 0) t.draw(t.start.get._1, y, x - t.start.get._1 , t.start.get._2 - y, t.fill, g)
      else if (x - t.start.get._1 < 0 && y - t.start.get._2 < 0) t.draw(x, y, t.start.get._1 - x, t.start.get._2 - y, t.fill, g)
      else t.draw(x, t.start.get._2, t.start.get._1 - x, y - t.start.get._2, t.fill, g)
    }
   
  }  
}
// g2.draw(new Ellipse2D.Double(x, y,rectwidth,rectheight));

case class EllipseReleaseCommand(x: Int, y: Int, t: PrettyEllipse) extends Command {
  
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y) && t.start.isDefined  && t.start.get != (x, y)) {
      g.setColor(t.color)
      g.setStroke(new BasicStroke(t.r))
      if (x - t.start.get._1 > 0 && y - t.start.get._2 > 0) t.draw(t.start.get._1, t.start.get._2, x - t.start.get._1 , y - t.start.get._2, t.fill, g)
      else if (x - t.start.get._1 > 0 && y - t.start.get._2 < 0) t.draw(t.start.get._1, y, x - t.start.get._1 , t.start.get._2 - y, t.fill, g)
      else if (x - t.start.get._1 < 0 && y - t.start.get._2 < 0) t.draw(x, y, t.start.get._1 - x, t.start.get._2 - y, t.fill, g)
      else t.draw(x, t.start.get._2, t.start.get._1 - x, y - t.start.get._2, t.fill, g)
    }

  }  
}
case class EllipsePressCommand(x: Int, y: Int, t: PrettyEllipse) extends Command {
  
  def execute(g: Graphics2D) = {
    t.start = Some((x, y))
  }
  
}
