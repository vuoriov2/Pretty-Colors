package tools
import java.awt.{ Graphics2D, Color}
import main.Canvas

class PrettyPencil(val r: Int, val color: java.awt.Color, val canvas: Canvas) extends Tool {
  def name = "Pencil"
  def findCommand(x: Int, y: Int, id: String): Option[Command] = {
    id match {
      case "Click" => Some(PencilClickCommand(x, y, this))
      case "Drag"  => Some(PencilDragCommand(x, y, this, true))
      case otherwise => None
    }
  }
}

case class PencilDragCommand(x: Int, y: Int, t: PrettyPencil, temp: Boolean = false) extends Command {
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y)) {
      g.setColor(t.color)
      g.fillOval(x - t.r/2, y - t.r/2, t.r, t.r)
    }
  }  
}
case class PencilClickCommand(x: Int, y: Int, t: PrettyPencil, temp: Boolean = false) extends Command {
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y)) {
      g.setColor(t.color)
      g.fillOval(x - t.r/2, y - t.r/2, t.r, t.r)
    }
  }  
}