package tools
import scala.collection.mutable.Buffer

object CommandList {
  
  private val cmd = Buffer[Command]()
  
  var temp: Option[Command] = None
  def setTemp(c: Command) = temp = Some(c)
  def clearTemp() = temp = None
  
  def get = cmd
  
  def undo() = {
    println("...")
    if (cmd.length > 0) cmd.remove(cmd.length - 1)
  }
  
  def clear() = cmd.clear()
  
  def add(c: Command) = cmd += c
  
  def deleteLast(c: Command) = {
    while (cmd.indexOf(c) != -1) cmd.remove(cmd.indexOf(c))
  }
  
  def deleteLastInstance(c: Command) = {
    var index = cmd.length - 1
    var searching = true
    while (searching && index >= 0) {
      if (cmd(index).toString == c.toString) {
        cmd.remove(index)
        searching = false
      }
      index -= 1
    }
  }
  
}