package tools
import java.awt.{ Graphics2D, Color}
import main.Canvas

class PrettyRectangle(val r: Int, val color: java.awt.Color, val fill: Boolean,  val canvas: Canvas) extends Tool {
  
  def name = "Rectangle"
  
  var start: Option[(Int, Int)] = None
  def setStart(x: Int, y: Int) = start = Some((x, y))
  
  def draw(x: Int, y: Int, width: Int, height: Int, fill: Boolean, g: Graphics2D) = {
    if (fill) g.fillRect(x, y, width, height)
    else g.drawRect(x, y, width, height)
  }
  
  def findCommand(x: Int, y: Int, id: String) = {
    id match {
      case "Press"   => Some(RectanglePressCommand(x, y, this))
      case "Release" => Some(RectangleReleaseCommand(x, y, this))
      case "Drag"    => Some(RectangleDragCommand(x, y, this))
      case otherwise => None
    }
  }
  
}

import java.awt.BasicStroke

case class RectangleDragCommand(x: Int, y: Int, t: PrettyRectangle) extends Command {

  override def isTemporary = true
  

  
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y) && t.start.isDefined) {
      g.setColor(t.color)
      g.setStroke(new BasicStroke(t.r))
      if (x - t.start.get._1 > 0 && y - t.start.get._2 > 0) t.draw(t.start.get._1, t.start.get._2, x - t.start.get._1 , y - t.start.get._2, t.fill, g)  
      else if (x - t.start.get._1 > 0 && y - t.start.get._2 < 0) t.draw(t.start.get._1, y, x - t.start.get._1 , t.start.get._2 - y, t.fill, g)
      else if (x - t.start.get._1 < 0 && y - t.start.get._2 < 0) t.draw(x, y, t.start.get._1 - x, t.start.get._2 - y, t.fill, g)
      else t.draw(x, t.start.get._2, t.start.get._1 - x, y - t.start.get._2, t.fill, g)
    }
   
  }  
}

case class RectangleReleaseCommand(x: Int, y: Int, t: PrettyRectangle) extends Command {
  
  def execute(g: Graphics2D) = {
    if (t.canvas.inBounds(x, y) && t.start.isDefined  && t.start.get != (x, y)) {
      g.setColor(t.color)
      g.setStroke(new BasicStroke(t.r))
      if (x - t.start.get._1 > 0 && y - t.start.get._2 > 0) t.draw(t.start.get._1, t.start.get._2, x - t.start.get._1 , y - t.start.get._2, t.fill, g)  
      else if (x - t.start.get._1 > 0 && y - t.start.get._2 < 0) t.draw(t.start.get._1, y, x - t.start.get._1 , t.start.get._2 - y, t.fill, g)
      else if (x - t.start.get._1 < 0 && y - t.start.get._2 < 0) t.draw(x, y, t.start.get._1 - x, t.start.get._2 - y, t.fill, g)
      else t.draw(x, t.start.get._2, t.start.get._1 - x, y - t.start.get._2, t.fill, g)
    }

  }  
}
case class RectanglePressCommand(x: Int, y: Int, t: PrettyRectangle) extends Command {
  
  def execute(g: Graphics2D) = {
    t.setStart(x, y)
  }
  
}
